<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>
        <link rel="stylesheet" href="css/styles.css">
    <title>Cyberlifecafe</title>

<head>

</head>
<body>

<br><br><br><br><br>
<div class="row row-cols-1 row-cols-md-3 g-4">
  <div class="col">
    <div class="card h-100">
      <img src="image/npru3.png" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">ม.ราชภัฏนครปฐม จัดพิธีซ้อมใหญ่ เตรียมเข้ารับพระราชทาน</h5>
        <p class="card-text">วันที่ 1 ก.ย. 65 เวลา 10.00 น. สมเด็จพระเจ้าลูกเธอ เจ้าฟ้าพัชรกิติยาภา เสด็จแทนพระองค์ พระราชทานปริญญาบัตรแก่บัณฑิตผู้สำเร็จการศึกษาจากมหาวิทยาลัยราชภัฏนครปฐม ประจำปีการศึกษา 2560-2562 ระดับปริญญาตรี </p>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card h-100">
      <img src="image/npru4.png" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">Card title</h5>
        <p class="card-text">This is a short card.</p>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card h-100">
      <img src="image/npru5.png" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">Card title</h5>
        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card h-100">
      <img src="image/npru6.png" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">Card title</h5>
        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
      </div>
    </div>
  </div>

    <div class="col">
    <div class="card h-100">
      <img src="image/npru7.png" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">มรน. จัดพิธีไหว้ครู “เฟื่องฟ้าบูชาครู” ของนักศึกษาภาค กศ.พป. ประจำปีการศึกษา 2565</h5>
        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
      </div>
    </div>
  </div>

    <div class="col">
    <div class="card h-100">
      <img src="image/npru8.png" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">มรน. จัดการแข่งขันกีฬา “เฟื่องฟ้าเกมส์ 65”</h5>
        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
      </div>
    </div>
  </div>

  
</div>
</body>
<br><br><br><br><br><br><br>